$(function(){
	$('#textFont').fontselect().change(function(){
		font = getFont();
		tobj = topCanvas.getActiveObject();
		tobj.set({
			fontFamily: font
		});
		topCanvas.renderAll();
	});
});

function getFont(){
	var font = $('#textFont').val().replace(/\+/g, ' ');
	font = font.split(':');
	return font[0];
}


/*$('.mycolorpicker').ColorPicker({
	color: '#0000ff',
	onShow: function (colpkr) {
		$(colpkr).fadeIn(500);
		return false;
	},
	onHide: function (colpkr) {
		$(colpkr).fadeOut(500);
		return false;
	},
	onChange: function (hsb, hex, rgb) {
		$('#colorSelector div').css('backgroundColor', '#' + hex);
	}
});*/

function changeTextColor(hex){
	tobj = topCanvas.getActiveObject();
	tobj.set({
		fill: '#'+hex
	});
	topCanvas.renderAll();
}

$('#textColor').ColorPicker({
	color: '#0000ff',
	onShow: function (colpkr) {
		$(colpkr).fadeIn(500);
		return false;
	},
	onHide: function (colpkr) {
		$(colpkr).fadeOut(500);
		return false;
	},
	onChange: function (hsb, hex, rgb) {
		//console.log(this);
		//target = $('.mycolorpicker').attr('id');
		//console.log(target);
		$('#textColor div').css('backgroundColor', '#' + hex);
		changeTextColor(hex);	
	}
});
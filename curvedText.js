var flag1 = 0;
var flag2 = 0;
var font1 = 'sans-serif';
var font2 = 'sans-serif';
var color1 = '#c33';
var color2 = '#c33';

$('#check1').change(function (){
	flag1 = ++flag1%2;
});
$('#check2').change(function (){
	flag2 = ++flag2%2;
});

function getFont2(){
	var temp = $('#curvedTextFont').val().replace(/\+/g, ' ');
	temp = temp.split(':');
	return temp[0];
}

$(function(){
	$('#curvedTextFont').fontselect().change(function(){
		if(flag1 == 1){
			font1 = getFont2();
			var canvas = $('#canvas3')
			canvas.clearCanvas();
			canvas.drawText({
				fillStyle: color1,
				fontFamily: font1,
				fontSize: 18,
				text: 'Heading 1',
				x: 410, y: 207,
				radius: 65,
				letterSpacing: 0.09,
			});
		}
		if(flag2 == 1){
			font2 = getFont2();
			var canvas = $('#canvas4')
			canvas.clearCanvas();
			canvas.drawText({
				fillStyle: color2,
				fontFamily: font2,
				fontSize: 18,
				text: 'Heading 1',
				x: 410, y: 207,
				radius: 65,
				letterSpacing: 0.09,
				rotate: 180
			});
		}
	});
});

$('#curved1').click(function(){
	var canvasBoard = $('.canvasBoard')[0];
	var newCanvas = document.createElement('canvas');
	canvasBoard.appendChild(newCanvas);
	newCanvas.setAttribute('class', 'mycanvas');
	canvasCount += 1
	id = 'canvas'+'3';
	newCanvas.setAttribute('id', id);
	newCanvas.style.zIndex = canvasCount;
	var temp = new fabric.Canvas(id, {position: 'absolute'});
	temp.setHeight($('.canvasBoard').height());
	temp.setWidth($('.canvasBoard').width());
	temp.selection = false;
	readjust();
	$('#'+id).drawText({
		fillStyle: '#c33',
		fontFamily: 'Ubuntu, sans-serif',
		fontSize: 18,
		text: 'Heading 1',
		x: 410, y: 207,
		radius: 65,
		letterSpacing: 0.09,
	});
});
$('#curved2').click(function(){
	var canvasBoard = $('.canvasBoard')[0];
	var newCanvas = document.createElement('canvas');
	canvasBoard.appendChild(newCanvas);
	newCanvas.setAttribute('class', 'mycanvas');
	canvasCount += 1
	id = 'canvas'+'4';
	newCanvas.setAttribute('id', id);
	newCanvas.style.zIndex = canvasCount;
	var temp = new fabric.Canvas(id, {position: 'absolute'});
	temp.setHeight($('.canvasBoard').height());
	temp.setWidth($('.canvasBoard').width());
	temp.selection = false;
	readjust();
	$('#'+id).drawText({
		fillStyle: '#c33',
		fontFamily: 'Ubuntu, sans-serif',
		fontSize: 18,
		text: 'Heading 1',
		x: 410, y: 207,
		radius: 65,
		letterSpacing: 0.09,
		rotate: 180
	});
});


$('#curvedTextColor').ColorPicker({
	color: '#0000ff',
	onShow: function (colpkr) {
		$(colpkr).fadeIn(500);
		return false;
	},
	onHide: function (colpkr) {
		$(colpkr).fadeOut(500);
		return false;
	},
	onChange: function (hsb, hex, rgb) {
		if(flag1 == 1){
			color1 = '#'+hex;
			var canvas = $('#canvas3')
			canvas.clearCanvas();
			canvas.drawText({
				fillStyle: color1,
				fontFamily: font1,
				fontSize: 18,
				text: 'Heading 1',
				x: 410, y: 207,
				radius: 65,
				letterSpacing: 0.09,
			});
		}if(flag2 == 1){
			color2 = '#'+hex;
			var canvas = $('#canvas4')
			canvas.clearCanvas();
			canvas.drawText({
				fillStyle: color2,
				fontFamily: font2,
				fontSize: 18,
				text: 'Heading 1',
				x: 410, y: 207,
				radius: 65,
				letterSpacing: 0.09,
				rotate: 180
			});
		}
		$('#curvedTextColor div').css('backgroundColor', '#' + hex);
	}
});
var canvasCount = 1;
var topLayerFlag = 0;
var topCanvas;
function readjust(){
	for (i=0; i<$('.canvas-container').size(); i++){
		$('.canvas-container')[i].style.position = "absolute";
	}
}

$('#buttonColor').ColorPicker({
	color: '#0000ff',
	onShow: function (colpkr) {
		$(colpkr).fadeIn(500);
		return false;
	},
	onHide: function (colpkr) {
		$(colpkr).fadeOut(500);
		return false;
	},
	onChange: function (hsb, hex, rgb) {
		$('#buttonColor div').css('backgroundColor', '#' + hex);
		$('#canvas1').clearCanvas();
		var radial = $('#canvas1').createGradient({
			x1: 410, y1:207,
			x2: 410, y2:207,
			r1: 72, r2: 80,
			c1: '#'+hex,
			c2: 'rgba(6, 6, 6, 0.26)'
		});
		$('#canvas1').drawArc({
			fillStyle: radial,
			x: 410, y: 207,
			radius: 80
		});
	}
});

$(function () {
	//$('#colorpicker').ColorPicker();
	canvas = new fabric.Canvas('canvas1', {position: 'absolute'});
	canvas.setHeight($('.canvasBoard').height());
	canvas.setWidth($('.canvasBoard').width());
	canvas.selection = false;
	readjust();
	var canvasBoard = $('.canvasBoard')[0];
	var newCanvas = document.createElement('canvas');
	canvasBoard.appendChild(newCanvas);
	newCanvas.setAttribute('class', 'mycanvas');
	canvasCount += 1
	id = 'canvas'+'2';
	newCanvas.setAttribute('id', id);
	newCanvas.style.zIndex = 99;
	topCanvas = new fabric.Canvas(id, {position: 'absolute'});
	topCanvas.setHeight($('.canvasBoard').height());
	topCanvas.setWidth($('.canvasBoard').width());
	topCanvas.selection = false;
	readjust();
	/*canvas.add(new fabric.Circle({
		radius: 80,
		fill: 'yellow',
		originX: 'center',
		originY: 'center',
		left: 410,
		top: 207,
		selectable: false
	}));*/
	var radial = $('#canvas1').createGradient({
		x1: 410, y1:207,
		x2: 410, y2:207,
		r1: 72, r2: 80,
		//c1: 'rgba(255, 0, 0, 0.75)',
		c1: '#FFC107',
		c2: 'rgba(6, 6, 6, 0.26)'
	});
	$('#canvas1').drawArc({
		fillStyle: radial,
		x: 410, y: 207,
		radius: 80
	});
	$('.draggable').draggable({
		opacity: 0.8,
		helper: function(event){
			return $(event.target).clone().css({
				width: $(event.target).width()
			});
		}
	});
	$('.canvasBoard').droppable({
		accept: '.draggable',
		drop: function(event, ui){
			var itemToAdd = $(ui.draggable);
			//console.log(itemToAdd[0].id);
			if (itemToAdd[0].id == 'text'){
				topCanvas.add(new fabric.Textbox('Tap and Type',{
					left: 50,
					top: 50,
					width: 150,
					fontSize: 20
				}));
			}
		}
	});
});

